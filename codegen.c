#include <stdio.h>

#include "9cc.h"

int labelX = 0;

void gen_lval(Node *node) {
  if (node->kind != ND_LVAR)
    error("left hand side must be a variable");
  printf("  mov x0, x29\n");
  printf("  sub x0, x0, %d\n", node->offset);
  printf("  str x0, [sp, -16]!\n");
}

void gen(Node *node) {
  int x;
  switch (node->kind) {
    case ND_RETURN:
      gen(node->lhs);
      printf("  ldr x0, [sp], 16\n");
      printf("  mov sp, x29\n");
      printf("  ldp x29, x30, [sp], 16\n");
      printf("  ret\n");
      return;
    case ND_BLOCK:
      for (Node *nd = node->next; nd; nd = nd->next) {
        gen(nd);
        if (nd->next)
          printf("  ldr x0, [sp], 16\n");
      }
      return;
    case ND_IF:
      x = labelX++;
      gen(node->lhs);
      printf("  ldr x0, [sp], 16\n");
      printf("  cmp x0, 0\n");
      printf("  str x0, [sp, -16]!\n");
      if (node->rhs2) // if node contains 'else'
        printf("  beq .Lelse%d\n", x);
      else
        printf("  beq .Lend%d\n", x);
      gen(node->rhs);
      if (node->rhs2) {// if node contains 'else'
        printf("  b .Lend%d\n", x);
        printf(".Lelse%d:\n", x);
        gen(node->rhs2);
      }
      printf(".Lend%d:\n", x);
      return;
    case ND_FOR:
      x = labelX++;
      if (node->lhs)
        gen(node->lhs);
      printf(".Lbegin%d:\n", x);
      if (node->lhs2) {
        gen(node->lhs2);
        printf("  ldr x0, [sp], 16\n");
        printf("  cmp x0, 0\n");
        printf("  str x0, [sp, -16]!\n");
        printf("  beq .Lend%d\n", x);
      }
      gen(node->rhs);
      if (node->lhs3)
        gen(node->lhs3);
      printf("  b .Lbegin%d\n", x);
      printf(".Lend%d:\n", x);
      return;
    case ND_WHILE:
      x = labelX++;
      printf(".Lbegin%d:\n", x);
      gen(node->lhs);
      printf("  ldr x0, [sp], 16\n");
      printf("  cmp x0, 0\n");
      printf("  str x0, [sp, -16]!\n");
      printf("  beq .Lend%d\n", x);
      gen(node->rhs);
      printf("  b .Lbegin%d\n", x);
      printf(".Lend%d:\n", x);
      return;
    case ND_NUM:
      printf("  mov x0, %d\n", node->val);
      printf("  str x0, [sp, -16]!\n");
      return;
    case ND_LVAR:
      gen_lval(node);
      printf("  ldr x0, [sp], 16\n");
      printf("  ldr x0, [x0]\n");
      printf("  str x0, [sp, -16]!\n");
      return;
    case ND_ASSIGN:
      gen_lval(node->lhs);
      gen(node->rhs);

      printf("  ldr x1, [sp], 16\n");
      printf("  ldr x0, [sp], 16\n");
      printf("  str x1, [x0]\n");
      printf("  str x1, [sp, -16]!\n");
      return;
  }

  gen(node->lhs);
  gen(node->rhs);

  printf("  ldr x1, [sp], 16\n");
  printf("  ldr x0, [sp], 16\n");

  switch (node->kind) {
    case ND_ADD:
      printf("  add x0, x0, x1\n");
      break;
    case ND_SUB:
      printf("  sub x0, x0, x1\n");
      break;
    case ND_MUL:
      printf("  mul x0, x0, x1\n");
      break;
    case ND_DIV:
      printf("  sdiv x2, x0, x1\n");
      printf("  mov x0, x2\n");
      break;
    case ND_EQ:
      printf("  cmp x0, x1\n");
      printf("  cset x0, eq\n");
      break;
    case ND_NE:
      printf("  cmp x0, x1\n");
      printf("  cset x0, ne\n");
      break;
    case ND_GT:
      printf("  cmp x0, x1\n");
      printf("  cset x0, gt\n");
      break;
    case ND_GE:
      printf("  cmp x0, x1\n");
      printf("  cset x0, ge\n");
      break;
    case ND_LT:
      printf("  cmp x0, x1\n");
      printf("  cset x0, lt\n");
      break;
    case ND_LE:
      printf("  cmp x0, x1\n");
      printf("  cset x0, le\n");
      break;
  }

  printf("  str x0, [sp, -16]!\n");
}
