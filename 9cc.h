typedef enum {
  TK_RESERVED,
  TK_IDENT,
  TK_NUM,
  TK_RETURN,
  TK_IF,
  TK_ELSE,
  TK_FOR,
  TK_WHILE,
  TK_EOF,
} TokenKind;

typedef struct Token Token;

struct Token {
  TokenKind kind;
  Token *next;
  int val;
  char *str;
  int len;
};

typedef struct Node Node;

typedef enum {
  ND_ADD,
  ND_SUB,
  ND_MUL,
  ND_DIV,
  ND_EQ,
  ND_NE,
  ND_GT,
  ND_GE,
  ND_LT,
  ND_LE,
  ND_ASSIGN,
  ND_LVAR,
  ND_NUM,
  ND_RETURN,
  ND_IF,
  ND_FOR,
  ND_WHILE,
  ND_BLOCK,
} NodeKind;

struct Node {
  NodeKind kind;
  Node *lhs;
  Node *lhs2;
  Node *lhs3;
  Node *rhs;
  Node *rhs2;
  Node *next;
  int val; // only used when kind is ND_NUM
  int offset; // for ND_LVAR
};

typedef struct LVar LVar;

struct LVar {
  LVar *next;
  char *name;
  int len;
  int offset;
};

extern Token *token;

extern LVar *locals;

extern char *user_input;

extern Node *code[];

void gen(Node *node);

void tokenize();

void error(char *fmt, ...);

void program();
