#!/bin/bash

assert() {
	expected="$1"
	input="$2"

	./9cc "$input" > tmp.s
	cc -o tmp tmp.s
	./tmp
	actual="$?"

	if [ "$actual" = "$expected" ]; then
		echo "$input => $actual"
	else
		echo "$input => $expected expected, but got $actual"
		exit 1
	fi
}

assert 0 "0;"
assert 42 "42;"
assert 21 "5+20-4;"
assert 41 " 12 + 34 - 5 ;"
assert 47 "5+6*7;"
assert 15 "5*(9-6);"
assert 4 "(3+5)/2;"
assert 10 "-10+20;"
assert 9 "+5-(-2*+2);"
assert 1 "3++(-2);"
assert 1 "3==1+2;"
assert 0 "3==9+2;"
assert 0 "5!=3+2;"
assert 1 "5!=9+2;"
assert 0 "3>1+3;"
assert 0 "3>1+2;"
assert 1 "3>1+1;"
assert 0 "3>=1+3;"
assert 1 "3>=1+2;"
assert 1 "3>=1+1;"
assert 0 "3<1+1;"
assert 0 "3<1+2;"
assert 1 "3<1+3;"
assert 0 "3<=1+1;"
assert 1 "3<=1+2;"
assert 1 "3<=1+3;"
assert 3 "a=3;"
assert 5 "a=2;b=3;a+b;"
assert 14 "a = 3; b = 5 * 6 - 8; a + b / 2;"
assert 3 "aaa=2; bbb=1; c=aaa+bbb;"
assert 1 "a=2;bb=2;ccc=3;dddd=4;zzzzz=1;"
assert 3 "return 3;"
assert 5 "return 5; return 8;"
assert 14 "a = 3; b = 5 * 6 - 8; return a + b / 2;"
assert 12 "a=0; while (a < 10) a = a + 3; return a;"
assert 3 "a = 3; if (4 == 5) a = 2; return a;"
assert 2 "a = 3; if (4 == 4) a = 2; return a;"
assert 3 "a = 4; if (2 == 3) a = 2; else a = 3; return a;"
assert 2 "a = 4; if (3 == 3) a = 2; else a = 3; return a;"
assert 13 "a = 10; for (i = 0; i < 3; i = i + 1) a = a + 1; return a;"
assert 10 "a = 10; for (;;) return a;"
assert 5 "a = 0; for (; a<5;) a = a + 1; return a;"
assert 30 "i=0; j=0; while(i<10) {j=j+2; i=i+1;} return i+j;"
assert 6 "{a=1;b=2;c=3;} return a+b+c;"

echo OK

