#include <stdio.h>

#include "9cc.h"

// current token
Token *token;

char *user_input;

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "number of argc not correct\n");
    return 1;
  }

  user_input = argv[1];
  tokenize();
  program();

  printf(".global main\n");
  printf("main:\n");

  // prologue and allocate space of 26 variables
  printf("  stp x29, x30, [sp, -16]!\n");
  printf("  mov x29, sp\n");
  if (locals)
    printf("  sub sp, sp, %d\n", locals->offset);

  for (int i = 0; code[i]; i++) {
    gen(code[i]);

    // load the result
    printf("  ldr x0, [sp], 16\n");
  }

  // epilogue
  printf("  mov sp, x29\n");
  printf("  ldp x29, x30, [sp], 16\n");
  printf("  ret\n");
  return 0;
}
